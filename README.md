# Yoshi's UserCSS collection

Project goals:
- make few changes which greatly improve productivity or usability
- ...with minimal selectors and the minimal number of rules (less LOC = fewer flaws)

Non-goals:
- adding dark themes to sites which lack one ([Dark Reader](https://github.com/darkreader/darkreader)'s [detect mode](https://github.com/darkreader/darkreader/discussions/9016) is great)
- doing anything on request; if I'll never use it myself I'm not going to bother

Click-to-install links require [Stylus](https://add0n.com/stylus.html) or similar.

### GitHub

[Click to install](https://gitlab.com/YoshiRulz/usercss-collection/-/raw/master/usercss/github_betterification.user.css)
for [GitHub](https://github.com) (SaaS).

Expands main content into margins, and other minor changes.

### Focus (various sites)

- [Click to install](https://gitlab.com/YoshiRulz/usercss-collection/-/raw/master/usercss/archwiki_focus.user.css) for [ArchWiki](https://wiki.archlinux.org).
- ~~Click to install for Genius.~~ Removed because their CSS is obfuscated and they keep changing it. I wrote a scraper anyway.
- [Click to install](https://gitlab.com/YoshiRulz/usercss-collection/-/raw/master/usercss/nebula_focus.user.css) for [Nebula](https://nebula.tv).
- [Click to install](https://gitlab.com/YoshiRulz/usercss-collection/-/raw/master/usercss/stackexchange_focus.user.css) for [StackExchange](https://stackexchange.com)/[StackOverflow](https://stackoverflow.com) and sister sites.
- [Click to install](https://gitlab.com/YoshiRulz/usercss-collection/-/raw/master/usercss/wolframalpha_focus.user.css) for [Wolfram|Alpha](https://wolframalpha.com).
- [Click to install](https://gitlab.com/YoshiRulz/usercss-collection/-/raw/master/usercss/xda_forums_focus.user.css) for [XDA Forums](https://forum.xda-developers.com).

Removes distractions and expands main content into margins.

### YouTube

[Click to install](https://gitlab.com/YoshiRulz/usercss-collection/-/raw/master/usercss/youtube_tweaks.user.css)
for [YouTube](https://youtube.com).

Cleans up the left pane, and other minor tweaks.

## Links

I also use two userscripts for YouTube (these require [TamperMonkey](https://www.tampermonkey.net) or similar):
- [Iridium](https://github.com/ParticleCore/Iridium)—not the current version but the final prerelease version, 0.2.5 (as it has many features which have since been removed): [click to install](https://github.com/ParticleCore/Iridium/raw/0.2.5/src/Userscript/Iridium.user.js)
- [Disable YouTube 60 FPS](https://greasyfork.org/en/scripts/23329-disable-youtube-60-fps-force-30-fps): [click to install](https://greasyfork.org/scripts/23329-disable-youtube-60-fps-force-30-fps/code/Disable%20YouTube%2060%20FPS%20(Force%2030%20FPS).user.js)

I also use [this userscript](https://greasyfork.org/en/scripts/382039-speed-up-google-captcha) which claims to remove delays in the reCAPTCHA v2 UI, though I'm not sure if it's actually doing anything. [click to install](https://greasyfork.org/scripts/382039-speed-up-google-captcha/code/Speed%20up%20Google%20Captcha.user.js)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received [a copy of the GNU General Public License](LICENSE.md) along with this program.  If not, see [gnu.org/licenses](https://www.gnu.org/licenses).
